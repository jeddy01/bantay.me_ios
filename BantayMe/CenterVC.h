//
//  CenterVC.h
//  BantayMe
//
//  Created by Oxygenventures iMac2 on 12/9/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftMenuVC.h"
#import "RightMenuVC.h"

@protocol CenterViewControllerDelegate <NSObject>

@optional
- (void)movePanelLeft;
- (void)movePanelRight;

@required
- (void)showSetPhoneNumberView;

@required
- (void)movePanelToOriginalPosition;

@end

@interface CenterVC : UIViewController <LeftPanelViewControllerDelegate, RightPanelViewControllerDelegate>

@property (nonatomic, assign) id<CenterViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

//@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftButton;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightButton;

@end

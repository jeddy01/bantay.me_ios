//
//  CountDownTimerDialogView.h
//  AlarmTutorial
//
//  Created by Oxygenventures iMac2 on 12/15/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CountDownTimerDialogViewDelegate;

@interface CountDownTimerDialogView : UIView

@property (nonatomic, weak) id<CountDownTimerDialogViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *countDownTimerContainerView;

@end

@protocol CountDownTimerDialogViewDelegate <NSObject>


-(void)countDownTimerDialogViewDidSelectTimeInterval:(NSTimeInterval ) interval;
-(void)countDownTimerDialogViewDidTapAlarmNow;
-(void)countDownTimerDialogViewDidDeactivateTimer;

@end
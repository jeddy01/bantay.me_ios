//
//  PhoneNumberDialogView.h
//  BantayMe
//
//  Created by ian on 12/13/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyPhoneNumberDialogViewDelegate;

@interface MyPhoneNumberDialogView : UIView
@property (weak, nonatomic) IBOutlet UIView *myPhoneNumberViewContainer;

@property (nonatomic, weak) id<MyPhoneNumberDialogViewDelegate> delegate;

@end

@protocol MyPhoneNumberDialogViewDelegate <NSObject>

-(void)phoneNumberDialogViewDidSetPhoneNumber:(NSString *) phoneNumber;

@end

//
//  PanicAlarmTriggerDialogView.h
//  BantayMe
//
//  Created by ian on 12/14/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PanicAlarmTriggerDialogViewDelegate;

@interface PanicAlarmTriggerDialogView : UIView

@property (nonatomic, weak) id<PanicAlarmTriggerDialogViewDelegate> delegate;
@property (nonatomic, assign) NSInteger defaultTime;
@property (weak, nonatomic) IBOutlet UIView *redUiView;

-(void) startCountdown;

@end

@protocol PanicAlarmTriggerDialogViewDelegate <NSObject>

-(void)panicAlarmTriggerDialogViewDidFinishCountdown;

-(void) panicAlarmTriggerDialogViewDidCancelCountdown;

@end
//
//  SetPhoneNumberDialogView.m
//  BantayMe
//
//  Created by ian on 12/13/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import "SetPhoneNumberDialogView.h"
#import "SettingsVC.h"

@interface SetPhoneNumberDialogView()

- (IBAction)btnSettingsAction:(UIButton *)sender;

@end

@implementation SetPhoneNumberDialogView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:[SetPhoneNumberDialogView description] owner:self options:nil];
        
        [self addSubview: (UIView*)[nibViews objectAtIndex:0]];
        
    }
    
    return self;
    
}

#pragma mark - IBActions

- (IBAction)btnSettingsAction:(UIButton *)sender {
    
    if([self.delegate respondsToSelector:@selector(setPhoneNumberDialogDidTapSettings)]){
        [self.delegate setPhoneNumberDialogDidTapSettings];
    }
    
    [self removeFromSuperview];
    
}
@end

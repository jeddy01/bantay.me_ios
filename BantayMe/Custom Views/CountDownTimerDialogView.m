//
//  CountDownTimerDialogView.m
//  AlarmTutorial
//
//  Created by Oxygenventures iMac2 on 12/15/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import "CountDownTimerDialogView.h"
#import "PureLayout.h"

@interface CountDownTimerDialogView()<UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *datePickerHours;
@property (weak, nonatomic) IBOutlet UIPickerView *datePickerMinutes;
@property (weak, nonatomic) IBOutlet UIButton *btnAlarmNow;
@property (weak, nonatomic) IBOutlet UIButton *btnDeactivateTimer;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;

- (IBAction)btnAlarmNowAction:(UIButton *)sender;
- (IBAction)btnOkAction:(UIButton *)sender;
- (IBAction)btnDeactivateTimerAction:(UIButton *)sender;

@end

@implementation CountDownTimerDialogView{
    NSMutableArray *hoursList;
    NSMutableArray *minutesList;
}
@synthesize delegate;

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"CountDownTimerDialogView" owner:self options:nil];
        
        [self addSubview: (UIView*)[nibViews objectAtIndex:0]];
        //[self addSubview:[nibs firstObject]];
        
        [self.datePickerHours setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
        [self.datePickerMinutes setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    
        
        //[self.datePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
//        SEL selector = NSSelectorFromString(@"setHighlightsToday:");
//        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDatePicker instanceMethodSignatureForSelector:selector]];
//        BOOL no = NO;
//        [invocation setSelector:selector];
//        [invocation setArgument:&no atIndex:2];
//        [invocation invokeWithTarget:self.datePicker];
        
        hoursList = [NSMutableArray array];
        minutesList = [NSMutableArray array];
        
        for(int i=0; i<24; i++){
            [hoursList addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        for(int i=0; i<60; i++){
            [minutesList addObject:[NSString stringWithFormat:@"%d", i]];
        }
        
        self.btnDeactivateTimer.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        NSArray *subviews = @[self.btnAlarmNow, self.btnDeactivateTimer, self.btnOk];
        [subviews autoDistributeViewsAlongAxis:ALAxisHorizontal alignedTo:ALAttributeHorizontal withFixedSpacing:5.0 insetSpacing:YES];

    }
    
    return self;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - UIPickerViewDelegate

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView == self.datePickerHours){
        return hoursList.count;
    }else{ //Minutes
        return minutesList.count;
    }
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSString *title = @"";
    if(pickerView == self.datePickerHours){
        title = [hoursList objectAtIndex:row];
    }else{ //Minutes
        title = [minutesList objectAtIndex:row];
    }
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}


#pragma mark - IBActions

- (IBAction)btnAlarmNowAction:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(countDownTimerDialogViewDidTapAlarmNow)]){
        [self.delegate countDownTimerDialogViewDidTapAlarmNow];
    }
    
    [self removeFromSuperview];
}

- (IBAction)btnOkAction:(UIButton *)sender {
    
    NSInteger hourRow = [self.datePickerHours selectedRowInComponent:0];
    NSInteger minuteRow = [self.datePickerMinutes selectedRowInComponent:0];
    
    NSInteger hourVal = [[hoursList objectAtIndex:hourRow] intValue];
    NSInteger minuteVal = [[minutesList objectAtIndex:minuteRow] intValue];
    
    NSTimeInterval timeInterval = (hourVal * 3600) + (minuteVal * 60);
    NSLog(@"CountDown Interval: %f", timeInterval);
    
    if (timeInterval > 0) {
        if([self.delegate respondsToSelector:@selector(countDownTimerDialogViewDidSelectTimeInterval:)]){
            [self.delegate countDownTimerDialogViewDidSelectTimeInterval:timeInterval];
        }
    }else{
        NSLog(@"Cannot set 0 NSTimeInterval value.");
    }
    
    [self removeFromSuperview];
}

- (IBAction)btnDeactivateTimerAction:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(countDownTimerDialogViewDidDeactivateTimer)]){
        [self.delegate countDownTimerDialogViewDidDeactivateTimer];
    }
    
    [self removeFromSuperview];
}
@end

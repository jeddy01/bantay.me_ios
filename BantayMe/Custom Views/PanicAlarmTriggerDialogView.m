//
//  PanicAlarmTriggerDialogView.m
//  BantayMe
//
//  Created by ian on 12/14/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import "PanicAlarmTriggerDialogView.h"
#import "Constants.h"

@interface PanicAlarmTriggerDialogView()

@property (weak, nonatomic) IBOutlet UILabel *lblRemainingSeconds;
- (IBAction)btnCancelAction:(UIButton *)sender;


@end

@implementation PanicAlarmTriggerDialogView{
    NSTimer *countDownTimer;
    NSInteger localTimeRemaining;
    NSInteger defaultTimeCopy;
}
@synthesize delegate;

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:[PanicAlarmTriggerDialogView description] owner:self options:nil];
        
        UIView *theView = (UIView*)[nibViews objectAtIndex:0];
        NSLog(@"The Tag: %d", theView.tag);
        [self addSubview: theView];
        self.defaultTime = COUNTDOWN_TIMER_DEFAULT_TIME;
        
        //[theView setTranslatesAutoresizingMaskIntoConstraints:YES];
        
        NSDictionary *redUiViewDict = @{@"redUiView" :  self.redUiView,
                                        
                                                        };
        NSDictionary *redUiViewMetricsDict = @{@"redUiViewWidth" : [NSNumber numberWithFloat:self.redUiView.frame.size.width] };

        NSLog(@"Width is : %f", self.redUiView.bounds.size.width);
        NSString *horizontalContraint = @"H:|-[redUiView]-|";
        NSString *verticalConstraint = @"V:|-[redUiView]-|";
        
        NSArray *layoutConstraintsVertical = [NSLayoutConstraint constraintsWithVisualFormat:verticalConstraint options:0 metrics:redUiViewMetricsDict views:redUiViewDict];
        NSArray *layoutConstraintsHorizontal = [NSLayoutConstraint constraintsWithVisualFormat:horizontalContraint options:0 metrics:redUiViewMetricsDict views:redUiViewDict];
        
//        [self addConstraints:layoutConstraintsHorizontal];
//        [self addConstraints:layoutConstraintsVertical];
//        [self.redUiView setNeedsUpdateConstraints];
        [self.redUiView layoutIfNeeded];
    }
    
    return self;
    
}

-(void)setDefaultTime:(NSInteger)defaultTime{
    localTimeRemaining = defaultTime;
    defaultTimeCopy = defaultTime;
}

-(NSInteger)defaultTime{
    return defaultTimeCopy;
}

-(void) updateUI:(NSTimer *) timer{
    
    if(localTimeRemaining >= 0){
        
        [self.lblRemainingSeconds setText:[NSString stringWithFormat:@"%ld", (long)localTimeRemaining]];
        localTimeRemaining--;
        
    }else{
        
        [countDownTimer invalidate];
        
        localTimeRemaining = self.defaultTime;
        if([self.delegate respondsToSelector:@selector(panicAlarmTriggerDialogViewDidFinishCountdown)]){
            [self.delegate panicAlarmTriggerDialogViewDidFinishCountdown];
        }
        
        [self removeFromSuperview];
    }
    
}

-(void)startCountdown{
    
    countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateUI:)
                                                    userInfo:nil
                                                     repeats:YES];
    
    [countDownTimer fire];
    
}

#pragma mark - IBActions
- (IBAction)btnCancelAction:(UIButton *)sender {
    
    [countDownTimer invalidate];
    
    if([self.delegate respondsToSelector:@selector(panicAlarmTriggerDialogViewDidCancelCountdown)]){
        [self.delegate panicAlarmTriggerDialogViewDidCancelCountdown];
    }
    
    [self removeFromSuperview];
}
@end

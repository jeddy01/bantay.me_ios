//
//  PhoneNumberDialogView.m
//  BantayMe
//
//  Created by ian on 12/13/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import "MyPhoneNumberDialogView.h"

@interface MyPhoneNumberDialogView()

@property (weak, nonatomic) IBOutlet UITextField *textFieldPhoneNumber;

- (IBAction)btnOkAction:(UIButton *)sender;

@end

@implementation MyPhoneNumberDialogView
@synthesize delegate;

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"MyPhoneNumberDialogView" owner:self options:nil];
        
        [self addSubview: (UIView*)[nibViews objectAtIndex:0]];
        
    }
    
    return self;
}

#pragma mark - IBActions
- (IBAction)btnOkAction:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(phoneNumberDialogViewDidSetPhoneNumber:)]){
        
        NSString *newPhoneNumber = self.textFieldPhoneNumber.text;
        
        if([newPhoneNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0){
            [self.delegate phoneNumberDialogViewDidSetPhoneNumber:newPhoneNumber];
        }
        
        [self removeFromSuperview];
    }
}
@end

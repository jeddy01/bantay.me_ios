//
//  SetPhoneNumberDialogView.h
//  BantayMe
//
//  Created by ian on 12/13/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SetPhoneNumberDialogViewDelegate;

@interface SetPhoneNumberDialogView : UIView

@property (nonatomic, weak) id<SetPhoneNumberDialogViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *setPhoneNumberContainerView;

@end

@protocol SetPhoneNumberDialogViewDelegate <NSObject>

-(void)setPhoneNumberDialogDidTapSettings;

@end

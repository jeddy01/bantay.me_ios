//
//  CustomAlertMessageDialogView.m
//  BantayMe
//
//  Created by ian on 12/14/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import "CustomAlertMessageDialogView.h"

@interface CustomAlertMessageDialogView()
- (IBAction)btnOkAction:(UIButton *)sender;

@end

@implementation CustomAlertMessageDialogView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:[CustomAlertMessageDialogView description] owner:self options:nil];
        
        [self addSubview: (UIView*)[nibViews objectAtIndex:0]];
        
    }
    
    return self;
    
}

#pragma mark - IBActions

- (IBAction)btnOkAction:(UIButton *)sender {
    
    [self removeFromSuperview];
    
}
@end

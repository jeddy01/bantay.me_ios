//
//  main.m
//  BantayMe
//
//  Created by Oxygenventures iMac2 on 12/9/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

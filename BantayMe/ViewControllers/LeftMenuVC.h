//
//  LeftMenuVC.h
//  BantayMe
//
//  Created by Oxygenventures iMac2 on 12/9/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol LeftPanelViewControllerDelegate <NSObject>

@optional
- (void)imageSelected:(UIImage *)image withTitle:(NSString *)imageTitle withCreator:(NSString *)imageCreator;

@required
//- (void)animalSelected:(Animal *)animal;

@end

@interface LeftMenuVC : UIViewController

@property (nonatomic, assign) id<LeftPanelViewControllerDelegate> delegate;
-(void) loginFacebookIfAllowed;

@end
//
//  LeftMenuVC.m
//  BantayMe
//
//  Created by Oxygenventures iMac2 on 12/9/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import "LeftMenuVC.h"
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "Constants.h"

@interface LeftMenuVC ()

@property (weak, nonatomic) IBOutlet UIButton *btnFacebookLogin;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewUserProfilePic;

- (IBAction)btnFacebookLoginAction:(UIButton *)sender;
- (IBAction)btnLogoutAction:(UIButton *)sender;

@end

@implementation LeftMenuVC{
    PFUser *currentUser;
}

#pragma mark -
#pragma mark View Did Load/Unload

- (void)viewDidLoad
{
    [super viewDidLoad];
    currentUser = [PFUser currentUser];
}

-(void) loginFacebookIfAllowed{
    currentUser = [PFUser currentUser];
    
    //Automatically Login Facebook if set
    if(currentUser){
        [self loginToFacebook];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

#pragma mark -
#pragma mark View Will/Did Appear

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark -
#pragma mark Default System Code

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Instance Methods

- (void)_presentUserDetailsViewControllerAnimated:(BOOL)animated {
    //UserDetailsViewController *detailsViewController = [[UserDetailsViewController alloc] initWithStyle:UITableViewStyleGrouped];
    //[self.navigationController pushViewController:detailsViewController animated:animated];
}

-(BOOL) isUserAlreadyExisting{
    BOOL isExisting = NO;
    
    
    
    return isExisting;
}

-(void) loginToFacebook{
    
    // Set permissions required from the facebook user account
    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location"];
    
    // Login PFUser using Facebook
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        
        NSLog(@"Activity Indicator Stop Animating...");
        //[_activityIndicator stopAnimating]; // Hide loading indicator
        
        if (!user) {
            NSString *errorMessage = nil;
            if (!error) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
                errorMessage = @"Uh oh. The user cancelled the Facebook login.";
            } else {
                NSLog(@"Uh oh. An error occurred: %@", error);
                errorMessage = [error localizedDescription];
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error"
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Dismiss", nil];
            [alert show];
        } else {
            
            currentUser = user;
            
            if (user.isNew) {
                NSLog(@"User with facebook signed up and logged in!");
                
            } else {
                NSLog(@"User with facebook logged in!");
            }
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSString *cachedPhoneNumber = (NSString *)[userDefaults objectForKey:KEY_USER_PHONE_NUMBER];
            
            if(!currentUser){
                [currentUser setObject:cachedPhoneNumber forKey:PARSE_KEY_USER_PHONE_NUMBER];
                [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    NSLog(@"Successfully updated phone number of Current User");
                }];
            }
            
            NSLog(@"Logged In User: %@", user);
            
            NSString *facebookId = [[[PFFacebookUtils session] accessTokenData] userID];
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?", facebookId]];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage *profilePic = [[UIImage alloc] initWithData:data];
            
            //[self.imgViewUserProfilePic setImage:profilePic];
            [self.btnFacebookLogin setImage:profilePic forState:UIControlStateNormal];
            [self.btnFacebookLogin setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
            //[self _presentUserDetailsViewControllerAnimated:YES];
        }
    }];
    
    NSLog(@"Activity Indicator Start Animating...");
    // [_activityIndicator startAnimating]; // Show loading indicator until login is finished
    
}

#pragma mark - IBActions
- (IBAction)btnFacebookLoginAction:(UIButton *)sender {
    
    [self loginToFacebook];
    
}

- (IBAction)btnLogoutAction:(UIButton *)sender {
    
    [PFUser logOut];
    [self.imgViewUserProfilePic setImage:nil];
}
@end
//
//  RightMenuVC.m
//  BantayMe
//
//  Created by Oxygenventures iMac2 on 12/9/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import "RightMenuVC.h"
#import <AddressBookUI/AddressBookUI.h>
#import "ContactsCollectionViewCell.h"
#import <CoreData/CoreData.h>
#import "Contact.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "Constants.h"
#import "HelperTool.h"

#define COLLECTION_CELL_ID @"contactCell"

@interface RightMenuVC ()<ABPeoplePickerNavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate>{
    
    NSManagedObjectContext *context;
    PFUser *currentUser;
    
}

@property (nonatomic, strong, readwrite) NSMutableArray *contactsList;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewContacts;
- (IBAction)btnContactsAction:(UIButton *)sender;

@end

@implementation RightMenuVC

#pragma mark -
#pragma mark View Did Load/Unload

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    context = appDelegate.managedObjectContext;
    currentUser = [PFUser currentUser];
    
    self.contactsList = [NSMutableArray array];
    [self.contactsList addObject:[NSNull null]];
    
    [self loadSavedContacts];
    
    //[self.collectionViewContacts registerClass:[ContactsCollectionViewCell class] forCellWithReuseIdentifier:COLLECTION_CELL_ID];
}

-(void) loadSavedContacts{
    
    NSEntityDescription *contactEntity = [NSEntityDescription entityForName:[Contact description] inManagedObjectContext:context];
    NSFetchRequest *contactsFetchRequest = [NSFetchRequest new];
    [contactsFetchRequest setEntity:contactEntity];
    
    NSError *error;
    NSArray *contactResults = [context executeFetchRequest:contactsFetchRequest error:&error];
    
    [self.contactsList removeAllObjects];
    [self.contactsList addObject:[NSNull null]];
    
    if(contactResults && contactResults.count > 0){
        for(Contact *contact in contactResults){
            [self.contactsList insertObject:contact atIndex:0];
        }
        [self.collectionViewContacts reloadData];
    }
    
}

#pragma mark -
#pragma mark Default System Code

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Instance Methods

- (IBAction)btnContactsAction:(UIButton *)sender {
    
    //[self presentViewController:picker animated:YES completion:nil];
    
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    
    [self presentViewController:picker animated:YES completion:nil];
//    [self presentModalViewController:picker animated:YES];
}

#pragma mark - ABPeoplePickerNavigationControllerDelegate

//-(void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person{
//
//    NSLog(@"%s",__PRETTY_FUNCTION__);
//    [self displayPerson:person];
//}

-(void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person property:property identifier:identifier];
    
}

- (void)peoplePickerNavigationControllerDidCancel:
(ABPeoplePickerNavigationController *)peoplePicker
{
    NSLog(@"Did Cancel");
    //[self dismissModalViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}


- (BOOL)peoplePickerNavigationController:
(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    //[self displayPerson:person];
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    return NO;
}

- (BOOL)peoplePickerNavigationController:
(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person
                                property:(ABPropertyID)property
                              identifier:(ABMultiValueIdentifier)identifier
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSString *invalidMessage = nil;
    
    if (property == kABPersonPhoneProperty) {
        
        ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
        
        for(CFIndex i = 0; i < ABMultiValueGetCount(multiPhones); i++) {
            if(identifier == ABMultiValueGetIdentifierAtIndex (multiPhones, i)) {
                
                NSString *mobileLabel = (__bridge NSString*)ABMultiValueCopyLabelAtIndex(multiPhones, i);
                
                //BOOL shouldGetPhoneNumber = [mobileLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel] || [mobileLabel isEqualToString:(NSString *)kABPersonPhoneMainLabel]   || [mobileLabel isEqualToString:(NSString *)kABPersonPhoneIPhoneLabel] || [mobileLabel isEqualToString:(NSString *)kABPersonPhoneHomeFAXLabel]|| [mobileLabel isEqualToString:(NSString *)kABPersonPhoneOtherFAXLabel]|| [mobileLabel isEqualToString:(NSString *)kABPersonPhoneWorkFAXLabel] || [mobileLabel isEqualToString:(NSString *) kABHomeLabel];
                
                BOOL shouldGetPhoneNumber = [mobileLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel] || [mobileLabel isEqualToString:(NSString *)kABPersonPhoneMainLabel]   || [mobileLabel isEqualToString:(NSString *)kABPersonPhoneIPhoneLabel];
                
                
                NSLog(@"Mobile Label: %@ Should Get Phone #:%d", mobileLabel, shouldGetPhoneNumber);
                
                //if(shouldGetPhoneNumber){
                    
                    CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                    CFRelease(multiPhones);
                    NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
                    CFRelease(phoneNumberRef);
                    
                    NSLog(@"Old Phone Number: %@", phoneNumber);
                    
                    //Process Phone Number
                    phoneNumber = [HelperTool processPhoneNumber:phoneNumber];
                    
                    NSLog(@"New Phone Number: %@", phoneNumber);
                    
                    if(phoneNumber.length != 10){
                        invalidMessage = @"Invalid Mobile Number";
                    }else{
                        NSLog(@"Goes here...");
                        NSString *firstName = (__bridge_transfer NSString*)ABRecordCopyValue(person,kABPersonFirstNameProperty);
                        //NSString *middleName = (__bridge_transfer NSString*)ABRecordCopyValue(person,kABPersonMiddleNameProperty);
                        NSString *lastName = (__bridge_transfer NSString*)ABRecordCopyValue(person,kABPersonLastNameProperty);
                        
                        NSEntityDescription *contactEntity = [NSEntityDescription entityForName:[Contact description] inManagedObjectContext:context];
                        
                        //Check for Duplicates
                        NSFetchRequest *contactFetchRequest = [NSFetchRequest new];
                        [contactFetchRequest setEntity:contactEntity];
                        
                        NSString *predicateString = [NSString stringWithFormat:@"contactNumber == '%@'", phoneNumber];
                        NSPredicate *contactPredicate = [NSPredicate predicateWithFormat:predicateString];
                        [contactFetchRequest setPredicate:contactPredicate];
                        
                        NSError *error;
                        NSArray *duplicateContactsResult = [context executeFetchRequest:contactFetchRequest error:&error];
                        
                        //Notify User that the contact number is already existing
                        if(duplicateContactsResult && duplicateContactsResult.count > 0){
                            
                            NSLog(@"Duplicate Contact Number %@", phoneNumber);
                            
                        }else{ //Save Only if it is not yet existing
                            
                            Contact *newContact = [[Contact alloc]initWithEntity:contactEntity insertIntoManagedObjectContext:context];
                            newContact.contactName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
                            newContact.contactNumber = phoneNumber;
                            
                            if(![context save:&error]){
                                NSLog(@"Saving Contact FAILED.");
                            }else{
                                NSLog(@"Saving Contacts... Really...");
                                [self.contactsList insertObject:newContact atIndex:0];
                                [self.collectionViewContacts reloadData];
                                
                                currentUser = [PFUser currentUser];
                                if(currentUser){
                                    
                                    PFQuery *emergencyContactQuery = [PFQuery queryWithClassName:PARSE_KEY_EMERGENCY_CONTACT];
                                    [emergencyContactQuery whereKey:PARSE_KEY_EMERGENCY_CONTACT_TRACKABLE equalTo:[PFUser currentUser]];
                                    [emergencyContactQuery whereKey:PARSE_KEY_EMERGENCY_CONTACT_TRACKED_BY equalTo:phoneNumber];
                                    
                                    [emergencyContactQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                                        
                                        NSInteger currentUserEmergencyContactsCount = objects.count;
                                        
                                        //TODO: Find this phoneNumber in User table
                                        //TODO: if this phoneNumber exists in User table, add this to EmergencyContact table.. TrackedBy should use the current User's PhoneNumber (get from UserDefaults)
                                        
                                        //Update if already exists in Parse
                                        if(currentUserEmergencyContactsCount > 0){
                                            for(PFObject *fetchedEmergencyContact in objects){
                                                
                                                NSString *trackedBy = [fetchedEmergencyContact valueForKey:PARSE_KEY_EMERGENCY_CONTACT_TRACKED_BY];
                                                
                                                if([trackedBy isEqualToString:phoneNumber]){
                                                    NSLog(@"Phone Number already Exists!");
                                                    [fetchedEmergencyContact setObject:currentUser forKey:PARSE_KEY_EMERGENCY_CONTACT_TRACKABLE];
                                                    [fetchedEmergencyContact saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                                        if(succeeded){
                                                            NSLog(@"Succesfully Updated Emergency Contact");
                                                        }
                                                    }];
                                                }
                                            }
                                        }else{ //Insert new Emergency Contact
                                            
                                            PFObject *emergencyContact  = [[PFObject alloc]initWithClassName:PARSE_KEY_EMERGENCY_CONTACT];
                                            [emergencyContact setObject:phoneNumber forKey:PARSE_KEY_EMERGENCY_CONTACT_TRACKED_BY];
                                            [emergencyContact setObject:[PFUser currentUser] forKey:PARSE_KEY_EMERGENCY_CONTACT_TRACKABLE];
                                            [emergencyContact saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                                if(succeeded){
                                                    NSLog(@"Successfully saved new emergency contact for User: %@", [PFUser currentUser]);
                                                }
                                            }];
                                            
                                        }
                                        
                                    }];
                                    
                                }else{
                                    NSLog(@"No Current User.");
                                }
                            }
                            
                        }
                        
                        NSLog(@"Phone Number: %@", phoneNumber);
                    }
                //}
            }
        }
        
        NSLog(@"Invalid Message: %@", invalidMessage);
        if(invalidMessage){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:invalidMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }else{
        NSLog(@"Error");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Cannot add this type of mobile number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    return NO;
}

- (void)displayPerson:(ABRecordRef)person
{
    NSString* name = (__bridge_transfer NSString*)ABRecordCopyValue(person,
                                                                    kABPersonFirstNameProperty);
    //self.firstName.text = name;
    NSLog(@"FirstName: %@", name);
    
    NSString* phone = nil;
    ABMultiValueRef phoneNumbers = ABRecordCopyValue(person,kABPersonPhoneProperty);
    
    if (ABMultiValueGetCount(phoneNumbers) > 0) {
        phone = (__bridge_transfer NSString*) ABMultiValueCopyValueAtIndex(phoneNumbers, 0);
    } else {
        phone = @"[None]";
    }
    //self.phoneNumber.text = phone;
    NSLog(@"Phone Number: %@", phone);
    
    //ABMultiValueRef mobilePhoneNumber = ABRecordCopyValue(person,kABPersonPhoneMobileLabel);
//    NSString *mobilePhone = (__bridge_transfer NSString*) ABRecordCopyValue(person, kABPersonPhoneMainLabel);
    
    
//    NSLog(@"Mobile Phone: %@", mobilePhone);
    
    CFRelease(phoneNumbers);
}


#pragma mark - UICollectionViewDataSource

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ContactsCollectionViewCell *cell = (ContactsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:COLLECTION_CELL_ID forIndexPath:indexPath];
    
    NSString *contactName = [self.contactsList objectAtIndex:indexPath.row];
    
    if(![contactName isEqual:[NSNull null]]){
        
        Contact *fetchedContact = (Contact *)[self.contactsList objectAtIndex:indexPath.row];
        
        if(fetchedContact){
            cell.contactName.text = fetchedContact.contactName;
        }
        
        cell.contactName.textColor = [UIColor whiteColor];
        [cell.backgroundImageView setImage:nil];
        [cell.imgViewBadge setImage:[UIImage imageNamed:@"ic_launcher"]];
        [cell.imgViewBadge setContentMode:UIViewContentModeScaleAspectFit];
        cell.tag = 1;
    }else{
        cell.contactName.text = @"";
        [cell.backgroundImageView setImage:[UIImage imageNamed:@"btn_add_normal"]];
        [cell.backgroundImageView setContentMode:UIViewContentModeScaleAspectFit];
        [cell.imgViewBadge setImage:nil];
        cell.tag = -1;
    }
    
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.contactsList.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Item Selected at: %ld is: %@", (long)indexPath.row, [self.contactsList objectAtIndex:indexPath.row]);
    
    if(indexPath.row == self.contactsList.count-1){
        
        //Add Contacts
        
        ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
        picker.peoplePickerDelegate = self;
        [self presentViewController:picker animated:YES completion:nil];
        
    }else{
        
        UIAlertController *alertDeleteContact = [UIAlertController alertControllerWithTitle:@"Delete Contact" message:@"Are you sure you want to delete your contact?" preferredStyle:UIAlertControllerStyleAlert];
        
        [alertDeleteContact addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [alertDeleteContact addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            
            NSLog(@"Before: %ld", self.contactsList.count);
            
            Contact *contactToDelete = [self.contactsList objectAtIndex:indexPath.row];
            NSString *contactNumberCopy = contactToDelete.contactNumber;
            [context deleteObject:contactToDelete];
            
           
            NSError *error;
            if([context save:&error]){
                NSLog(@"Successfully Deleted Contact from local database.");
                
                [self.contactsList removeObject:contactToDelete];
                [self.collectionViewContacts reloadData];
                
                if(currentUser){
                    
                    //Delete object from Parse
                    PFQuery *emergencyContactQuery = [PFQuery queryWithClassName:PARSE_KEY_EMERGENCY_CONTACT];
                    [emergencyContactQuery whereKey:PARSE_KEY_EMERGENCY_CONTACT_TRACKABLE equalTo:currentUser];
                    [emergencyContactQuery whereKey:PARSE_KEY_EMERGENCY_CONTACT_TRACKED_BY equalTo:contactNumberCopy];
                    [emergencyContactQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                        [PFObject deleteAllInBackground:objects block:^(BOOL succeeded, NSError *error) {
                            NSLog(@"Succesfully Delete %ld Contact(s) from Parse", objects.count);
                        }];
                    }];
                    
                }else{
                    NSLog(@"Unable to delete Contact from Parse.. No Current User.");
                }
            }else{
                NSLog(@"Deletion Failed from local database.");
            }
            
        }]];
        
        [self presentViewController:alertDeleteContact animated:YES completion:nil];
        
    }
    
}



@end




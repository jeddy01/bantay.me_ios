//
//  SettingsVC.m
//  BantayMe
//
//  Created by ian on 12/13/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import "SettingsVC.h"
#import "MyPhoneNumberDialogView.h"
#import "Constants.h"
#import <Parse/Parse.h>
#import "HelperTool.h"

@interface SettingsVC ()<MyPhoneNumberDialogViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *labelPhoneNumber;
- (IBAction)btnPhoneNumberAction:(UIButton *)sender;
- (IBAction)btnBackAction:(UIButton *)sender;

@end

@implementation SettingsVC{
    MyPhoneNumberDialogView *myPhoneNumberDialogView;
    NSUserDefaults *userDefaults;
    UIAlertView *alertViewError;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    alertViewError = [[UIAlertView alloc]initWithTitle:@"Error" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    NSString *cachedUserPhoneNumber = (NSString *)[userDefaults objectForKey:KEY_USER_PHONE_NUMBER];
    if(cachedUserPhoneNumber){
        [self.lblPhoneNumber setText:cachedUserPhoneNumber];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PhoneNumberDialogViewDelegate
-(void)phoneNumberDialogViewDidSetPhoneNumber:(NSString *)phoneNumber{
    
    if(phoneNumber.length >= 10 && phoneNumber.length <= 13){
        phoneNumber = [HelperTool processPhoneNumber:phoneNumber];
        
        [userDefaults setObject:phoneNumber forKey:KEY_USER_PHONE_NUMBER];
        [userDefaults synchronize];
        
        PFUser *currentUser = [PFUser currentUser];
        if(currentUser){
            [currentUser setObject:phoneNumber forKey:PARSE_KEY_USER_PHONE_NUMBER];
            [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if(succeeded && !error){
                    NSLog(@"Successfully updated phone number of Current User");
                }else{
                    NSLog(@"Error updating user phone number: %@", [error localizedDescription]);
                }
            }];
        }else{
            NSLog(@"Settings VC: No Current User");
        }
        
        [self.lblPhoneNumber setText:phoneNumber];
    }else{
        [alertViewError setMessage:@"Invalid Phone Number"];
        [alertViewError show];
    }
    
    
}

#pragma mark - IBActions
- (IBAction)btnPhoneNumberAction:(UIButton *)sender {
    NSLog(@"Phone Number Tapped");
    myPhoneNumberDialogView = [[MyPhoneNumberDialogView alloc]initWithFrame:self.view.frame];
    
    [myPhoneNumberDialogView setDelegate:self];
    //myPhoneNumberDialogView.translatesAutoresizingMaskIntoConstraints = NO;

    [self.view addSubview:myPhoneNumberDialogView];

    NSDictionary *myPhoneNumberDialogViewDict = @{@"myPhoneNumberViewContainer" : myPhoneNumberDialogView.myPhoneNumberViewContainer
                                                      };
    NSDictionary *myPhoneNumberDialogViewMetricsDict = @{@"myPhoneNumberDialogViewWidth" : [NSNumber numberWithFloat: myPhoneNumberDialogView.bounds.size.width - 15]
                                                             };
    NSLog(@"Width is : %f", myPhoneNumberDialogView.bounds.size.width);
    NSString *horizontalContraint = @"H:|-[myPhoneNumberViewContainer(<=myPhoneNumberDialogViewWidth)]-|";
    NSString *verticalConstraint = @"V:|-(>=30)-[myPhoneNumberViewContainer]-(>=30)-|";
    
    NSArray *layoutConstraintsVertical = [NSLayoutConstraint constraintsWithVisualFormat:verticalConstraint options:0 metrics:nil views:myPhoneNumberDialogViewDict];
    NSArray *layoutConstraintsHorizontal = [NSLayoutConstraint constraintsWithVisualFormat:horizontalContraint options:0 metrics:myPhoneNumberDialogViewMetricsDict views:myPhoneNumberDialogViewDict];
    
    //
    [self.view addConstraints:layoutConstraintsHorizontal];
    [self.view addConstraints:layoutConstraintsVertical];

    
    /*
    NSLog(@"Count Constraints: %d", myPhoneNumberDialogView.testImageView.constraints.count);
    
    //[myPhoneNumberDialogView addConstraints:myPhoneNumberDialogView.viewLayoutConstraints];
    
    [self.view addSubview:myPhoneNumberDialogView];
    
    
    NSLog(@"Layout Constraints Count: %lu",(unsigned long)myPhoneNumberDialogView.viewLayoutConstraints.count);
    NSDictionary *myPhoneNumberDialogViewDict = @{@"myPhoneNumberDialogView" : myPhoneNumberDialogView
                                                  };
    NSDictionary *myPhoneNumberDialogViewMetricsDict = @{@"myPhoneNumberDialogViewWidth" : [NSNumber numberWithFloat: myPhoneNumberDialogView.bounds.size.width]
                                                         };
    NSLog(@"Width is : %f", myPhoneNumberDialogView.bounds.size.width);
    NSString *horizontalContraint = @"H:|[myPhoneNumberDialogView]|";
    NSString *verticalConstraint = @"V:|[myPhoneNumberDialogView]|";

    NSArray *layoutConstraintsVertical = [NSLayoutConstraint constraintsWithVisualFormat:verticalConstraint options:0 metrics:myPhoneNumberDialogViewMetricsDict views:myPhoneNumberDialogViewDict];
    NSArray *layoutConstraintsHorizontal = [NSLayoutConstraint constraintsWithVisualFormat:horizontalContraint options:0 metrics:myPhoneNumberDialogViewMetricsDict views:myPhoneNumberDialogViewDict];
    
//    
    [self.view addConstraints:layoutConstraintsHorizontal];
    [self.view addConstraints:layoutConstraintsVertical];

     */
//    [myPhoneNumberDialogView setNeedsUpdateConstraints];
//    [self.view setNeedsUpdateConstraints];
 
}
- (IBAction)btnBackAction:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end

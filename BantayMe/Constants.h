//
//  Constants.h
//  BantayMe
//
//  Created by ian on 12/13/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <Foundation/Foundation.h>

#define KEY_USER_PHONE_NUMBER @"user_phone_number"
#define KEY_USER_CURRENT_LOC_LAT @"user_current_loc_lat"
#define KEY_USER_CURRENT_LOC_LONG @"user_current_loc_long"

#define COUNTDOWN_TIMER_DEFAULT_TIME 5
#define UPDATE_LOCATION_TIME_INTERVAL 30

#define PARSE_KEY_USER_PHONE_NUMBER @"PhoneNumber"
#define PARSE_KEY_USER_TRACK_ME @"TrackMe"
#define PARSE_KEY_USER_PROFILE_PIC @"ProfilePic"
#define PARSE_KEY_USER_LAST_KNOWN_LOCATION @"LastKnownLocation"

#define PARSE_KEY_EMERGENCY_CONTACT @"EmergencyContact"
#define PARSE_KEY_EMERGENCY_CONTACT_TRACKABLE @"Trackable"
#define PARSE_KEY_EMERGENCY_CONTACT_TRACKED_BY @"TrackedBy"

@interface Constants : NSObject

@end

//
//  ViewController.m
//  BantayMe
//
//  Created by Oxygenventures iMac2 on 12/9/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import "MainViewController.h"
#import "RightMenuVC.h"
#import "LeftMenuVC.h"
#import "CenterVC.h"
#import <QuartzCore/QuartzCore.h>
#import <Parse/Parse.h>

#import "SetPhoneNumberDialogView.h"
#import "SettingsVC.h"

#define CENTER_TAG 1
#define LEFT_PANEL_TAG 2
#define RIGHT_PANEL_TAG 3
#define CORNER_RADIUS 4
#define SLIDE_TIMING .25
#define PANEL_WIDTH 60

@interface MainViewController ()<UIGestureRecognizerDelegate, CenterViewControllerDelegate, SetPhoneNumberDialogViewDelegate>

//Center View
@property (nonatomic, strong) CenterVC *centerVC;

//Left Panel View
@property (nonatomic, strong) LeftMenuVC *leftMenuVC;
@property (nonatomic, assign) BOOL showingLeftMenu;

//Right Panel View
@property (nonatomic, strong) RightMenuVC *rightMenuVC;
@property (nonatomic, assign) BOOL showingRightMenu;

//Move Panel Properties
@property (nonatomic, assign) BOOL showPanel;
@property (nonatomic, assign) CGPoint preVelocity;

@end

@implementation MainViewController

#pragma mark -
#pragma mark View Did Load/Unload

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    
    /* LEFT PANEL */
    
    // this is where you define the view for the left panel
    self.leftMenuVC = (LeftMenuVC *)[storyboard instantiateViewControllerWithIdentifier:@"LeftMenuVC"];
    self.leftMenuVC.view.tag = LEFT_PANEL_TAG;
    self.leftMenuVC.delegate = _centerVC;
    
    [_leftMenuVC loginFacebookIfAllowed];
    
    /* RIGHT PANEL */
    
    // this is where you define the view for the right panel
    self.rightMenuVC = (RightMenuVC *)[storyboard instantiateViewControllerWithIdentifier:@"RightMenuVC"];
    self.rightMenuVC.view.tag = RIGHT_PANEL_TAG;
    self.rightMenuVC.delegate = _centerVC;
  
    [self setupView];
    //[self loadAllDataFromCloud];
}

#pragma mark - Instance Methods

-(void)loadAllDataFromCloud{
    
    PFUser *currentUser = [PFUser currentUser];
    PFRelation *personRelation = [currentUser relationForKey:@"Person"];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Person"];
    //PFQuery *query = [personRelation query];
    
    [query setLimit:1000];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSLog(@"Load Objects Succeeded With Count: %ld, Error: %@", (unsigned long)objects.count, error);
    }];
    
}

#pragma mark -
#pragma mark Setup View

- (void)setupView
{
    // setup center view
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    
    self.centerVC = (CenterVC *)[storyboard instantiateViewControllerWithIdentifier:@"CenterVC"];
    self.centerVC.view.tag = CENTER_TAG;
    self.centerVC.delegate = self;
    
    [self.view addSubview:self.centerVC.view];
    [self addChildViewController:_centerVC];
    
    [self.centerVC didMoveToParentViewController:self];
    
    [self setupGestures];
}

- (void)showCenterViewWithShadow:(BOOL)value withOffset:(double)offset
{
    
    if (value)
    {
        [_centerVC.view.layer setCornerRadius:CORNER_RADIUS];
        [_centerVC.view.layer setShadowColor:[UIColor blackColor].CGColor];
        [_centerVC.view.layer setShadowOpacity:0.8];
        [_centerVC.view.layer setShadowOffset:CGSizeMake(offset, offset)];
        
    }
    else
    {
        [_centerVC.view.layer setCornerRadius:0.0f];
        [_centerVC.view.layer setShadowOffset:CGSizeMake(offset, offset)];
    }
    
}

- (void)resetMainView
{
    
    // remove left and right views, and reset variables, if needed
    if (_leftMenuVC != nil)
    {
        [self.leftMenuVC.view setHidden:YES];
        //[self.leftMenuVC.view removeFromSuperview];
        //self.leftMenuVC = nil;
        
        _centerVC.leftButton.tag = 1;
        self.showingLeftMenu = NO;
    }
    
    if (_rightMenuVC != nil)
    {
        //[self.rightMenuVC.view removeFromSuperview];
        //self.rightMenuVC = nil;
        [self.rightMenuVC.view setHidden:YES];
        
        _centerVC.rightButton.tag = 1;
        self.showingRightMenu = NO;
    }
    
    // remove view shadows
    [self showCenterViewWithShadow:NO withOffset:0];
    
}

- (UIView *)getLeftView
{
    // init view if it doesn't already exist
    if (_leftMenuVC == nil)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        // this is where you define the view for the left panel
        self.leftMenuVC = (LeftMenuVC *)[storyboard instantiateViewControllerWithIdentifier:@"LeftMenuVC"];
        self.leftMenuVC.view.tag = LEFT_PANEL_TAG;
        self.leftMenuVC.delegate = _centerVC;
        
    }
    
    [self.view addSubview:self.leftMenuVC.view];
    
    [self addChildViewController:_leftMenuVC];
    [_leftMenuVC didMoveToParentViewController:self];
    
    _leftMenuVC.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self.leftMenuVC.view setHidden:NO];
    self.showingLeftMenu = YES;
    
    // set up view shadows
    [self showCenterViewWithShadow:YES withOffset:-2];
    
    UIView *view = self.leftMenuVC.view;
    return view;
}

- (UIView *)getRightView
{
    // init view if it doesn't already exist
    if (_rightMenuVC == nil)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        // this is where you define the view for the right panel
        self.rightMenuVC = (RightMenuVC *)[storyboard instantiateViewControllerWithIdentifier:@"RightMenuVC"];
        self.rightMenuVC.view.tag = RIGHT_PANEL_TAG;
        self.rightMenuVC.delegate = _centerVC;
        
    }
    
    [self.view addSubview:self.rightMenuVC.view];
    
    [self addChildViewController:self.rightMenuVC];
    [_rightMenuVC didMoveToParentViewController:self];
    
    _rightMenuVC.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

    
    [self.rightMenuVC.view setHidden:NO];
    self.showingRightMenu = YES;
    
    // set up view shadows
    [self showCenterViewWithShadow:YES withOffset:2];
    
    UIView *view = self.rightMenuVC.view;
    return view;
}

#pragma mark -
#pragma mark Swipe Gesture Setup/Actions

#pragma mark - setup

- (void)setupGestures
{
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movePanel:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [panRecognizer setDelegate:self];
    
    [_centerVC.view addGestureRecognizer:panRecognizer];
    
}

-(void)movePanel:(id)sender
{
    
    [[[(UITapGestureRecognizer*)sender view] layer] removeAllAnimations];
    
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
    CGPoint velocity = [(UIPanGestureRecognizer*)sender velocityInView:[sender view]];
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        UIView *childView = nil;
        
        if(velocity.x > 0) {
            if (!_showingRightMenu) {
                childView = [self getLeftView];
            }
        } else {
            if (!_showingLeftMenu) {
                childView = [self getRightView];
            }
            
        }
        // Make sure the view you're working with is front and center.
        [self.view sendSubviewToBack:childView];
        [[sender view] bringSubviewToFront:[(UIPanGestureRecognizer*)sender view]];
    }
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        
        if(velocity.x > 0) {
            // NSLog(@"gesture went right");
        } else {
            // NSLog(@"gesture went left");
        }
        
        if (!_showPanel) {
            [self movePanelToOriginalPosition];
        } else {
            if (_showingLeftMenu) {
                [self movePanelRight];
            }  else if (_showingRightMenu) {
                [self movePanelLeft];
            }
        }
    }
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged) {
        if(velocity.x > 0) {
            // NSLog(@"gesture went right");
        } else {
            // NSLog(@"gesture went left");
        }
        
        // Are you more than halfway? If so, show the panel when done dragging by setting this value to YES (1).
        _showPanel = abs([sender view].center.x - _centerVC.view.frame.size.width/2) > _centerVC.view.frame.size.width/2;
        
        // Allow dragging only in x-coordinates by only updating the x-coordinate with translation position.
        [sender view].center = CGPointMake([sender view].center.x + translatedPoint.x, [sender view].center.y);
        [(UIPanGestureRecognizer*)sender setTranslation:CGPointMake(0,0) inView:self.view];
        
        // If you needed to check for a change in direction, you could use this code to do so.
        if(velocity.x*_preVelocity.x + velocity.y*_preVelocity.y > 0) {
            // NSLog(@"same direction");
        } else {
            // NSLog(@"opposite direction");
        }
        
        _preVelocity = velocity;
    }
    
}

#pragma mark -
#pragma mark Delegate Actions

- (void)movePanelLeft // to show right panel
{

    UIView *childView = [self getRightView];
    [self.view sendSubviewToBack:childView];
    
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         _centerVC.view.frame = CGRectMake(-self.view.frame.size.width + PANEL_WIDTH, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         NSLog(@"isFinished: %d", finished);
                         if (finished) {
                             
                             _centerVC.rightButton.tag = 0;
                         }
                     }];
    
}

- (void)movePanelRight // to show left panel
{
    NSLog(@"Moving Panel Left");
    UIView *childView = [self getLeftView];
    [self.view sendSubviewToBack:childView];
    
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         _centerVC.view.frame = CGRectMake(self.view.frame.size.width - PANEL_WIDTH, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         NSLog(@"isFinished: %d", finished);
                         if (finished) {
                             
                             _centerVC.leftButton.tag = 0;
                         }
                     }];
    
}

- (void)movePanelToOriginalPosition
{
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         _centerVC.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             
                             [self resetMainView];
                         }
                     }];
    
}

-(void)showSetPhoneNumberView{
    
    SetPhoneNumberDialogView *setPhoneNumberDialogView = [[SetPhoneNumberDialogView alloc]initWithFrame:self.view.bounds];
    [setPhoneNumberDialogView setDelegate:self];
    //[setPhoneNumberDialogView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.view addSubview:setPhoneNumberDialogView];
    
    
    NSDictionary *setPhoneNumberDialogViewDict = @{@"setPhoneNumberContainerView" : setPhoneNumberDialogView.setPhoneNumberContainerView
                                                      };
    NSDictionary *setPhoneNumberDialogViewMetricsDict = @{@"setPhoneNumberDialogViewWidth" : [NSNumber numberWithFloat: setPhoneNumberDialogView.bounds.size.width - 15]
                                                             };
    NSLog(@"Width is : %f", setPhoneNumberDialogView.bounds.size.width);
    NSString *horizontalContraint = @"H:|-[setPhoneNumberContainerView(<=setPhoneNumberDialogViewWidth)]-|";
    NSString *verticalConstraint = @"V:|-(>=30)-[setPhoneNumberContainerView]-(>=30)-|";
    
    NSArray *layoutConstraintsVertical = [NSLayoutConstraint constraintsWithVisualFormat:verticalConstraint options:0 metrics:nil views:setPhoneNumberDialogViewDict];
    NSArray *layoutConstraintsHorizontal = [NSLayoutConstraint constraintsWithVisualFormat:horizontalContraint options:0 metrics:setPhoneNumberDialogViewMetricsDict views:setPhoneNumberDialogViewDict];
    
    //
    [self.view addConstraints:layoutConstraintsHorizontal];
    [self.view addConstraints:layoutConstraintsVertical];

    
    /*
    NSDictionary *setPhoneNumberDialogViewDict = @{@"setPhoneNumberDialogView" : setPhoneNumberDialogView
                                                  };
    NSDictionary *setPhoneNumberDialogViewMetricsDict = @{@"setPhoneNumberDialogView" : [NSNumber numberWithFloat: setPhoneNumberDialogView.bounds.size.width]
                                                         };
    NSLog(@"Width is : %f", setPhoneNumberDialogView.bounds.size.width);
    NSString *horizontalContraint = @"H:|[setPhoneNumberDialogView]|";
    NSString *verticalConstraint = @"V:|[setPhoneNumberDialogView]|";
    
    NSArray *layoutConstraintsVertical = [NSLayoutConstraint constraintsWithVisualFormat:verticalConstraint options:0 metrics:setPhoneNumberDialogViewMetricsDict views:setPhoneNumberDialogViewDict];
    NSArray *layoutConstraintsHorizontal = [NSLayoutConstraint constraintsWithVisualFormat:horizontalContraint options:0 metrics:setPhoneNumberDialogViewMetricsDict views:setPhoneNumberDialogViewDict];
    
    [self.view addConstraints:layoutConstraintsHorizontal];
    [self.view addConstraints:layoutConstraintsVertical];
     */

}

#pragma mark - SetPhoneNumberDialogViewDelegate

-(void)setPhoneNumberDialogDidTapSettings{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *settingsVC = [storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
    [self presentViewController:settingsVC animated:YES completion:nil];
    
    //[self presentViewController:settingsVC animated:YES completion:nil];
}

#pragma mark -
#pragma mark Default System Code

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end

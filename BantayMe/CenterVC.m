//
//  CenterVC.m
//  BantayMe
//
//  Created by Oxygenventures iMac2 on 12/9/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import "CenterVC.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Constants.h"
#import "CustomAlertMessageDialogView.h"
#import <AudioToolbox/AudioToolbox.h>
#import "PanicAlarmTriggerDialogView.h"
#import "CountDownTimerDialogView.h"
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import <Parse/Parse.h>
#import <CoreData/CoreData.h>
#import "Contact.h"
#import "AppDelegate.h"
#import "HelperTool.h"
#import "PureLayout.h"
#import "MyPhoneNumberDialogView.h"

@import AVFoundation;

@interface CenterVC ()<CLLocationManagerDelegate, PanicAlarmTriggerDialogViewDelegate, CountDownTimerDialogViewDelegate>
@property (weak, nonatomic) IBOutlet GMSMapView *googleMapView;
@property (weak, nonatomic) IBOutlet UIView *mapContainerView;
- (IBAction)btnTimerAction:(UIButton *)sender;
- (IBAction)btnLocateAction:(UIButton *)sender;
- (IBAction)btnPanicAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnLocate;
@property (weak, nonatomic) IBOutlet UIButton *btnPanic;
@property (weak, nonatomic) IBOutlet UIButton *btnTimer;

@end

@implementation CenterVC{
    
    CLLocationManager *locationManager;
    //GMSMapView *mapView;
    NSUserDefaults *userDefaults;
    
    //For displaying custom messages
    CustomAlertMessageDialogView *customAlertMessageDialogView;
    
    //For playing panic siren sound
    AVAudioPlayer *audioPlayer;
    
    PFUser *currentUser;
    BOOL isLocateBtnSelected;
    NSTimer *timer;
    
    NSManagedObjectContext *context;
    NSMutableArray *contactsList;
}

#pragma mark -
#pragma mark View Did Load/Unload

- (void)viewDidLoad
{
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];
    currentUser = [PFUser currentUser];
    isLocateBtnSelected = NO;
    contactsList = [NSMutableArray array];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    context = appDelegate.managedObjectContext;
    
    customAlertMessageDialogView = [[CustomAlertMessageDialogView alloc]initWithFrame:self.view.bounds];
    
//    [self.btnPanic.imageView setContentMode:UIViewContentModeScaleAspectFit];
//    [self.btnTimer.imageView setContentMode:UIViewContentModeScaleAspectFit];
//    [self.btnLocate.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    [self initLocationManager];
    [self initGoogleMapView];
}

#pragma mark - Instance Methods

-(void) initLocationManager{
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        [locationManager requestWhenInUseAuthorization];
        //[locationManager requestAlwaysAuthorization];
    }else{
        [self requestCurrentLocationOfUser];
    }
    
}

-(void) initGoogleMapView{
    
    // Create a GMSCameraPosition that tells the map to display the
    // coordinate -33.86,151.20 at zoom level 6.
    GMSCameraPosition *initialCamera = [GMSCameraPosition cameraWithLatitude:11.587669
                                                            longitude:123.574219
                                                                 zoom:5];
    
    //self.googleMapView = [GMSMapView mapWithFrame:self.googleMapView.bounds camera:initialCamera];
    [self.googleMapView setCamera:initialCamera];
    self.googleMapView.myLocationEnabled = YES;
    self.googleMapView.mapType = kGMSTypeNormal;
    self.googleMapView.indoorEnabled = NO;
    //[self.mapContainerView addSubview: mapView];
    
    /*
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
    marker.title = @"Sydney";
    marker.snippet = @"Australia";
    marker.map = mapView;
     */
    
}

-(void) displayMapWithNewLocation:(CLLocation *) newLocation{
    NSLog(@"Focusing Camera on New Location: %f, %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                                    longitude:newLocation.coordinate.longitude
                                                                         zoom:16];
    
    [self.googleMapView setCamera:cameraPosition];
    
    /*
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    marker.title = @"You";
    //marker.snippet = @"Australia";
    marker.map = mapView;
     */
}


- (void)scheduleAlarmForDate:(NSDate*)theDate
{
    UIApplication* app = [UIApplication sharedApplication];
    NSArray*    oldNotifications = [app scheduledLocalNotifications];
    
    // Clear out the old notification before scheduling a new one.
    if ([oldNotifications count] > 0){
        [app cancelAllLocalNotifications];
    }
    
    // Create a new notification.
    UILocalNotification* alarm = [[UILocalNotification alloc] init];
    
    if (alarm){
        
        alarm.fireDate = theDate;
        alarm.timeZone = [NSTimeZone defaultTimeZone];
        alarm.repeatInterval = 0;
        alarm.applicationIconBadgeNumber = 1;
        alarm.alertBody = @"Time's Up!";
        
        alarm.soundName = @"siren_bantay.aiff";
        [app scheduleLocalNotification:alarm];
        
        //NSString *soundFilePath = [[NSBundle mainBundle] pathForResource: @"siren_bantay" ofType: @"mp3"];
        //alarm.soundName = UILocalNotificationDefaultSoundName;
        
    }
}
-(void) playPanicSound{
    NSLog(@"%@", @"playPanicSound");
    //NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/siren_bantay.mp3", [[NSBundle mainBundle] resourcePath]]];
    
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource: @"siren_bantay"
                                                              ofType: @"mp3"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: fileURL
                                                                      error: nil];
    [audioPlayer setVolume:1.0f];
    [audioPlayer prepareToPlay];
    [audioPlayer play];

}

-(void) showPanicAlarmTriggerDialogView{
    PanicAlarmTriggerDialogView *panicAlarmTriggerDialogView = [[PanicAlarmTriggerDialogView alloc]initWithFrame:self.view.bounds];
    [panicAlarmTriggerDialogView setDelegate:self];
    //[panicAlarmTriggerDialogView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.view addSubview:panicAlarmTriggerDialogView];
    
    
    NSDictionary *panicAlarmTriggerDialogViewDict = @{@"redUiView" : panicAlarmTriggerDialogView.redUiView
                                                      };
    NSDictionary *panicAlarmTriggerDialogViewMetricsDict = @{@"panicAlarmTriggerDialogViewWidth" : [NSNumber numberWithFloat: panicAlarmTriggerDialogView.bounds.size.width - 10]
                                                             };
    NSLog(@"Width is : %f", panicAlarmTriggerDialogView.bounds.size.width);
    NSString *horizontalContraint = @"H:|-[redUiView(<=panicAlarmTriggerDialogViewWidth)]-|";
    NSString *verticalConstraint = @"V:|-(>=30)-[redUiView]-(>=30)-|";
    
    NSArray *layoutConstraintsVertical = [NSLayoutConstraint constraintsWithVisualFormat:verticalConstraint options:0 metrics:nil views:panicAlarmTriggerDialogViewDict];
    NSArray *layoutConstraintsHorizontal = [NSLayoutConstraint constraintsWithVisualFormat:horizontalContraint options:0 metrics:panicAlarmTriggerDialogViewMetricsDict views:panicAlarmTriggerDialogViewDict];
    
    //
    [self.view addConstraints:layoutConstraintsHorizontal];
    [self.view addConstraints:layoutConstraintsVertical];
    [panicAlarmTriggerDialogView setClipsToBounds:YES];
    [panicAlarmTriggerDialogView startCountdown];
}

#pragma mark - ViewController LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [locationManager stopUpdatingLocation];
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - CountDownTimerDialogViewDeleagate

-(void)countDownTimerDialogViewDidDeactivateTimer{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    UIApplication* app = [UIApplication sharedApplication];
    NSArray*    oldNotifications = [app scheduledLocalNotifications];
    
    if ([oldNotifications count] > 0){
        NSLog(@"Removing %ld local notifications", oldNotifications.count);
        [app cancelAllLocalNotifications];
    }
    
}

-(void)countDownTimerDialogViewDidSelectTimeInterval:(NSTimeInterval)interval{
    NSDate *targetDate = [NSDate dateWithTimeInterval:interval sinceDate:[NSDate new]];
    [self scheduleAlarmForDate:targetDate];
}

-(void)countDownTimerDialogViewDidTapAlarmNow{
    [self countDownTimerDialogViewDidDeactivateTimer];
    [self showPanicAlarmTriggerDialogView];
    //[self playPanicSound];
}

#pragma mark -
#pragma mark Button Actions

- (IBAction)btnMovePanelRight:(id)sender
{
    UIButton *button = sender;
    switch (button.tag) {
        case 0: {
            [_delegate movePanelToOriginalPosition];
            break;
        }
            
        case 1: {
            [_delegate movePanelRight];
            break;
        }
            
        default:
            break;
    }
    
}

- (IBAction)btnMovePanelLeft:(id)sender
{
    UIButton *button = sender;
    switch (button.tag) {
        case 0: {
            [_delegate movePanelToOriginalPosition];
            break;
        }
            
        case 1: {
            [_delegate movePanelLeft];
            break;
        }
            
        default:
            break;
    }
    
}

#pragma mark -
#pragma mark Default System Code

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Schedule Methods

- (void)scheduleNotificationForDate:(NSDate *)date
{
    // Here we cancel all previously scheduled notifications
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    localNotification.fireDate = date;
    NSLog(@"Notification will be shown on: %@",localNotification.fireDate);
    
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.alertBody = [NSString stringWithFormat:@"Your notification message"];
    localNotification.alertAction = NSLocalizedString(@"View details", nil);
    
    /* Here we set notification sound and badge on the app's icon "-1"
     means that number indicator on the badge will be decreased by one
     - so there will be no badge on the icon */
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber = -1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

#pragma mark  - CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    switch(status){
        case kCLAuthorizationStatusAuthorizedAlways:{
            NSLog(@"Authorization Authorized Always");
        }
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:{
            NSLog(@"Authorization AuthorizedWhenInUse %@", locationManager);
            [locationManager startUpdatingLocation];
        }
            break;
        case kCLAuthorizationStatusDenied:{
            NSLog(@"Authorization Denied");
            [customAlertMessageDialogView.labelCustomMessage setText:@"Unable to access current location. Please make sure that Location services is enabled and allowed for this app. You may visit Settings > Privacy > Location."];
            //[customAlertMessageDialogView setTranslatesAutoresizingMaskIntoConstraints:NO];
            
            [self.view addSubview:customAlertMessageDialogView];
            
            NSDictionary *customAlertMessageDialogViewDict = @{@"customMessageContainerView" : customAlertMessageDialogView.customMessageContainerView
                                                              };
            NSDictionary *customAlertMessageDialogViewMetricsDict = @{@"customAlertMessageDialogViewWidth" : [NSNumber numberWithFloat: customAlertMessageDialogView.bounds.size.width - 15]
                                                                     };
            NSLog(@"Width is : %f", customAlertMessageDialogView.bounds.size.width);
            NSString *horizontalContraint = @"H:|-[customMessageContainerView(<=customAlertMessageDialogViewWidth)]-|";
            NSString *verticalConstraint = @"V:|-(>=30)-[customMessageContainerView]-(>=30)-|";
            
            NSArray *layoutConstraintsVertical = [NSLayoutConstraint constraintsWithVisualFormat:verticalConstraint options:0 metrics:nil views:customAlertMessageDialogViewDict];
            NSArray *layoutConstraintsHorizontal = [NSLayoutConstraint constraintsWithVisualFormat:horizontalContraint options:0 metrics:customAlertMessageDialogViewMetricsDict views:customAlertMessageDialogViewDict];
            
            //
            [self.view addConstraints:layoutConstraintsHorizontal];
            [self.view addConstraints:layoutConstraintsVertical];
            
        }
            break;
        case kCLAuthorizationStatusNotDetermined:{
            NSLog(@"Authorization Not Determined");
        }
            break;
        case kCLAuthorizationStatusRestricted:{
            NSLog(@"Authorization Restricted");
            [customAlertMessageDialogView.labelCustomMessage setText:@"Unable to get current location. Location Authorization is Restricted."];
            //[customAlertMessageDialogView setTranslatesAutoresizingMaskIntoConstraints:NO];
            
            [self.view addSubview:customAlertMessageDialogView];
            
            NSDictionary *customAlertMessageDialogViewDict = @{@"customMessageContainerView" : customAlertMessageDialogView.customMessageContainerView
                                                               };
            NSDictionary *customAlertMessageDialogViewMetricsDict = @{@"customAlertMessageDialogViewWidth" : [NSNumber numberWithFloat: customAlertMessageDialogView.bounds.size.width - 15]
                                                                      };
            NSLog(@"Width is : %f", customAlertMessageDialogView.bounds.size.width);
            NSString *horizontalContraint = @"H:|-[customMessageContainerView(<=customAlertMessageDialogViewWidth)]-|";
            NSString *verticalConstraint = @"V:|-(>=30)-[customMessageContainerView]-(>=30)-|";
            
            NSArray *layoutConstraintsVertical = [NSLayoutConstraint constraintsWithVisualFormat:verticalConstraint options:0 metrics:nil views:customAlertMessageDialogViewDict];
            NSArray *layoutConstraintsHorizontal = [NSLayoutConstraint constraintsWithVisualFormat:horizontalContraint options:0 metrics:customAlertMessageDialogViewMetricsDict views:customAlertMessageDialogViewDict];
            
            //
            [self.view addConstraints:layoutConstraintsHorizontal];
            [self.view addConstraints:layoutConstraintsVertical];
            
        }
            break;
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    NSLog(@"%s Error: %@", __PRETTY_FUNCTION__, error);
    
    //[customAlertMessageDialogView.labelCustomMessage setText:[error localizedDescription]];
    //[self.view addSubview:customAlertMessageDialogView];
    
    [self restartRequestCurrentLocationOfUser];
   
    
}

-(void) restartRequestCurrentLocationOfUser{
    
    if(timer){
        [timer invalidate];
    }
    
    //Request again after some time interval
    timer = [NSTimer scheduledTimerWithTimeInterval:UPDATE_LOCATION_TIME_INTERVAL target:self selector:@selector(requestCurrentLocationOfUser) userInfo:nil repeats:YES];
    [locationManager stopUpdatingLocation];

    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *newLocation = [locations lastObject];
    
    NSNumber *newLatitude = [NSNumber numberWithDouble:newLocation.coordinate.latitude];
    NSNumber *newLongitude = [NSNumber numberWithDouble:newLocation.coordinate.longitude];
    
    NSLog(@"User: %@ Updated his location at: %@, %@", currentUser, newLatitude, newLongitude);
    
    [userDefaults setObject:[NSNumber numberWithDouble:newLocation.coordinate.latitude] forKey:KEY_USER_CURRENT_LOC_LAT];
    [userDefaults setObject:[NSNumber numberWithDouble:newLocation.coordinate.longitude] forKey:KEY_USER_CURRENT_LOC_LONG];
    [userDefaults synchronize];
    
    if(currentUser){
        PFGeoPoint *newGeoPoint = [PFGeoPoint new];
        newGeoPoint.latitude = [newLatitude doubleValue];
        newGeoPoint.longitude = [newLongitude doubleValue];
        
        [currentUser setObject:newGeoPoint forKey:PARSE_KEY_USER_LAST_KNOWN_LOCATION];
        [currentUser saveEventually:^(BOOL succeeded, NSError *error) {
            if(succeeded && !error){
                NSLog(@"Current user's location is successfully SAVED to Parse");
            }else{
                NSLog(@"Something's Wrong! %@", error);
            }
        }];
    }
    
    //Display on map the current location of user
    [self displayMapWithNewLocation:newLocation];
    [self restartRequestCurrentLocationOfUser];

}

-(void) requestCurrentLocationOfUser{
    NSLog(@"Requesting Current Location of User...");
    [locationManager startUpdatingLocation];
    
    //Fetch data from parse and show to user who's tracking him/her
    [self showUsersTrackingMe];
    
    //Update location of current user to Parse
    
    NSFetchRequest *emergencyContactsRequest = [NSFetchRequest new];
    NSEntityDescription *contactsEntity = [NSEntityDescription entityForName:[Contact description] inManagedObjectContext:context];
    [emergencyContactsRequest setEntity:contactsEntity];
    
    NSError *error;
    NSArray *allContacts = [context executeFetchRequest:emergencyContactsRequest error:&error];
    
    if(allContacts && allContacts.count > 0){
        //Get contacts from Parse and display where they at.
        NSString *cachedUserPhoneNumber = (NSString *)[userDefaults objectForKey:KEY_USER_PHONE_NUMBER];
        PFQuery *emergencyContactsQuery = [PFQuery queryWithClassName:PARSE_KEY_EMERGENCY_CONTACT];
        if(cachedUserPhoneNumber){
            [emergencyContactsQuery whereKey:PARSE_KEY_EMERGENCY_CONTACT_TRACKED_BY equalTo:cachedUserPhoneNumber];
        }
        
        [emergencyContactsQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            NSMutableArray *trackedByUserList = [NSMutableArray array];
            [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                [trackedByUserList addObject:(PFObject *)obj];
                
            }];
            
            if(trackedByUserList.count > 0){
                PFQuery *usersQuery = [PFUser query];
                for(PFObject *fetchedObj in trackedByUserList){
                    
                    PFUser *fetchedUser = [((PFUser *) fetchedObj) valueForKey:PARSE_KEY_EMERGENCY_CONTACT_TRACKABLE];
                    NSString *fetchedUserObjectId = fetchedUser.objectId;
                    [usersQuery whereKey:@"objectId" equalTo:fetchedUserObjectId];
                    
                }
                
                [usersQuery findObjectsInBackgroundWithBlock:^(NSArray *objects1, NSError *error) {
                    
                    //Clear all markers
                    //[self.googleMapView clear];
                    
//                    [objects1 enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//                        
//                        PFUser *userObj = (PFUser *) obj;
//                        
//                        NSString *contactNumber = (NSString *)[userObj valueForKey:PARSE_KEY_USER_PHONE_NUMBER];
//                        contactNumber = [HelperTool processPhoneNumber:contactNumber];
//                        
//                        NSLog(@"The Contact #: %@", contactNumber);
//                        
//                        NSString *predicateString = [NSString stringWithFormat:@"contactNumber == '%@'", contactNumber];
//                        NSArray *filteredContacts = [allContacts filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:predicateString]];
//                        
//                        PFGeoPoint *geoPoint = (PFGeoPoint *) [userObj valueForKey:PARSE_KEY_USER_LAST_KNOWN_LOCATION];
//                        if(geoPoint){
//                            GMSMarker *marker = [[GMSMarker alloc] init];
//                            marker.position = CLLocationCoordinate2DMake(geoPoint.latitude, geoPoint.longitude);
//                            marker.title = ((Contact *)[filteredContacts lastObject]).contactName;
//                            //marker.snippet = @"Australia";
//                            marker.map = self.googleMapView;
//                        }
//                        
//                    }];
                    
                }];
            }
            
        }];
        
    }

}

-(void) showUsersTrackingMe{
    
    //Clear Map First
    [self.googleMapView clear];
    
    //Load saved emergency contacts for reference in displaying name in Map
    [self loadSavedContacts];
    
    //Locate friends... Get from Parse API
    NSString *cachedUserPhoneNumber = (NSString *)[userDefaults objectForKey:KEY_USER_PHONE_NUMBER];
    NSLog(@"Should display in map the friends that are tracking YOU.");
    
    if(cachedUserPhoneNumber){
        
        PFQuery *emergencyContactQuery = [PFQuery queryWithClassName:PARSE_KEY_EMERGENCY_CONTACT];
        [emergencyContactQuery whereKey:PARSE_KEY_EMERGENCY_CONTACT_TRACKED_BY equalTo:cachedUserPhoneNumber];
        [emergencyContactQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            NSLog(@"Number of users Tracking You: %ld", (long)objects.count);
            [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                //Get the Trackable column row-value
                PFUser *emergencyContact = (PFUser *)[obj objectForKey:PARSE_KEY_EMERGENCY_CONTACT_TRACKABLE];
                
                PFQuery *emergencyContactQuery2 = [PFUser query];
                [emergencyContactQuery2 whereKey:@"objectId" equalTo:emergencyContact.objectId];
                [emergencyContactQuery2 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        
                        PFUser *userTrackingYou = (PFUser *) obj;
                        BOOL trackMe = [[userTrackingYou valueForKey:PARSE_KEY_USER_TRACK_ME] boolValue];
                        NSLog(@"Track Me Of User:%@ is: %d", userTrackingYou.username, trackMe);
                        if(trackMe){
                            
                            PFGeoPoint *geoPoint = (PFGeoPoint *) [userTrackingYou valueForKey:PARSE_KEY_USER_LAST_KNOWN_LOCATION];
                            if(geoPoint){
                                NSLog(@"%@ is tracking YOU. His Last Known Location is %f, %f", userTrackingYou.username, geoPoint.latitude, geoPoint.longitude);
                                
                                GMSMarker *marker = [[GMSMarker alloc] init];
                                marker.position = CLLocationCoordinate2DMake(geoPoint.latitude, geoPoint.longitude);
                                
                                NSString *predicateString = [NSString stringWithFormat:@"contactNumber = '%@'", [userTrackingYou valueForKey:PARSE_KEY_USER_PHONE_NUMBER]];
                                NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:predicateString];
                                NSArray *searchResult = [self->contactsList filteredArrayUsingPredicate:searchPredicate];
                                
                                //Search for the contact number from the Contacts saved in our database
                                if(searchResult && searchResult.count > 0){
                                    
                                    Contact *filteredContact = (Contact *)[searchResult firstObject];
                                    marker.title =  filteredContact.contactName;
                                    
                                }else{ //Just use the number if the contact doesn't exist in our database
                                    marker.title =  [userTrackingYou objectForKey:PARSE_KEY_USER_PHONE_NUMBER];
                                }
                                
                                marker.map = self.googleMapView;
                            }
                        }
                        
                    }];
                    
                }];
                
            }];
            
        }];
    }
}

-(void) loadSavedContacts{
    
    NSEntityDescription *contactEntity = [NSEntityDescription entityForName:[Contact description] inManagedObjectContext:context];
    NSFetchRequest *contactsFetchRequest = [NSFetchRequest new];
    [contactsFetchRequest setEntity:contactEntity];
    
    NSError *error;
    NSArray *contactResults = [context executeFetchRequest:contactsFetchRequest error:&error];
    
    [self->contactsList removeAllObjects];
    [self->contactsList addObject:[NSNull null]];
    
    if(contactResults && contactResults.count > 0){
        for(Contact *contact in contactResults){
            [self->contactsList insertObject:contact atIndex:0];
        }
    }
    
}

#pragma mark - PanicAlarmTriggerDialogViewDelegate

-(void)panicAlarmTriggerDialogViewDidFinishCountdown{
    NSLog(@"Finished Countdown");
    [self playPanicSound];
    
}

-(void)panicAlarmTriggerDialogViewDidCancelCountdown{
    NSLog(@"Cancelled Countdown");
}


#pragma mark - IBActions

- (IBAction)btnTimerAction:(UIButton *)sender {
    
    CountDownTimerDialogView *countDownTimerDialogView = [[CountDownTimerDialogView alloc]initWithFrame:self.view.frame];
    [countDownTimerDialogView setDelegate:self];

    //NSLog(@"Self View Size: %@", NSStringFromCGRect(self.view.frame));
    //NSLog(@"CountDownTimerDialogView Size: %@", NSStringFromCGRect(countDownTimerDialogView.frame));
    
    //UIView *testUiView = [[UIView alloc]initWithFrame:self.view.bounds];
    //[testUiView setBackgroundColor:[UIColor greenColor]];
    //[self.view addSubview:testUiView];
    
    [self.view addSubview:countDownTimerDialogView];
    
    
    NSDictionary *countDownTimerContainerViewDict = @{@"countDownTimerContainerView" : countDownTimerDialogView.countDownTimerContainerView
                                                       };
    NSDictionary *countDownTimerContainerViewMetricsDict = @{@"countDownTimerContainerViewWidth" : [NSNumber numberWithFloat: countDownTimerDialogView.bounds.size.width - 15]
                                                              };
    NSLog(@"Width is : %f", countDownTimerDialogView.frame.size.width - 15);
    NSString *horizontalContraint = @"H:|-[countDownTimerContainerView(<=countDownTimerContainerViewWidth)]-|";
    NSString *verticalConstraint = @"V:|-(>=30)-[countDownTimerContainerView]-(>=30)-|";
    
    NSArray *layoutConstraintsVertical = [NSLayoutConstraint constraintsWithVisualFormat:verticalConstraint options:0 metrics:nil views:countDownTimerContainerViewDict];
    NSArray *layoutConstraintsHorizontal = [NSLayoutConstraint constraintsWithVisualFormat:horizontalContraint options:0 metrics:countDownTimerContainerViewMetricsDict views:countDownTimerContainerViewDict];
    
    [self.view addConstraints:layoutConstraintsHorizontal];
    [self.view addConstraints:layoutConstraintsVertical];
    [countDownTimerDialogView layoutIfNeeded];

}

- (IBAction)btnLocateAction:(UIButton *)sender {
    
    NSString *cachedUserPhoneNumber = (NSString *)[userDefaults objectForKey:KEY_USER_PHONE_NUMBER];
    
    if(!cachedUserPhoneNumber){
        
        //Show dialog indicating that PhoneNumber isn't set yet
        if([self.delegate respondsToSelector:@selector(showSetPhoneNumberView)]){
            [self.delegate showSetPhoneNumberView];
        }
        
        
    }else{
        
        isLocateBtnSelected = !isLocateBtnSelected;
        [self.btnLocate setSelected:isLocateBtnSelected];
        
        //Update User's TrackMe flag
        if(currentUser){
            [currentUser setObject:[NSNumber numberWithBool:isLocateBtnSelected] forKey:PARSE_KEY_USER_TRACK_ME];
            [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                NSLog(@"Updated current user's TrackMe column!");
            }];
        }
        
        if(isLocateBtnSelected){
            [self.btnLocate setImage:[UIImage imageNamed:@"btn_track_normal"] forState:UIControlStateHighlighted];
            //[self showUsersTrackingMe];
            
        }else{
            [self.btnLocate setImage:[UIImage imageNamed:@"btn_track_selected"] forState:UIControlStateHighlighted];
            
            [self.googleMapView clear];
        }
        
        NSLog(@"Current User: %@", [PFUser currentUser]);
    }
    
}

- (IBAction)btnPanicAction:(UIButton *)sender {
    
    [self showPanicAlarmTriggerDialogView];
}

@end

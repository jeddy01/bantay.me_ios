//
//  HelperTool.h
//  BantayMe
//
//  Created by Oxygenventures iMac2 on 12/16/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HelperTool : NSObject

+(NSString *) processPhoneNumber:(NSString *) phoneNumber;

@end

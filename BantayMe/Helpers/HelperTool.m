//
//  HelperTool.m
//  BantayMe
//
//  Created by Oxygenventures iMac2 on 12/16/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import "HelperTool.h"

@implementation HelperTool

+(NSString *) processPhoneNumber:(NSString *) phoneNumber{
    NSString *finalNumber = phoneNumber;
    
    //Process Phone Number
    for(NSString *charToReplace in @[@"(", @")", @"-", @"+", @" ", @"*", @"#"]){
        finalNumber = [finalNumber stringByReplacingOccurrencesOfString:charToReplace withString:@""];
    }
    
    if([finalNumber characterAtIndex:0] == '6' && [finalNumber characterAtIndex:1] == '3'){
        finalNumber = [finalNumber substringFromIndex:1];
    }
    
    NSRange validPhoneRange = NSMakeRange(finalNumber.length - 10, finalNumber.length-1);
    finalNumber = [finalNumber substringWithRange:validPhoneRange];

    NSLog(@"Final Number: %@", finalNumber);
    
    return finalNumber;
}

@end

//
//  CustomAlertMessageDialogView.h
//  BantayMe
//
//  Created by ian on 12/14/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertMessageDialogView : UIView
- (IBAction)btnOkAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelCustomMessage;
@property (weak, nonatomic) IBOutlet UIView *customMessageContainerView;

@end

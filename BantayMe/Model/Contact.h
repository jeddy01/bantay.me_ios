//
//  Contact.h
//  BantayMe
//
//  Created by Oxygenventures iMac2 on 12/15/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Contact : NSManagedObject

@property (nonatomic, retain) NSString * contactName;
@property (nonatomic, retain) NSString * contactNumber;

@end

//
//  ContactsCollectionViewCell.h
//  BantayMe
//
//  Created by ian on 12/6/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *contactName;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBadge;

@end

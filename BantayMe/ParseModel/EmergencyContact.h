//
//  EmergencyContact.h
//  BantayMe
//
//  Created by Oxygenventures iMac2 on 12/15/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface EmergencyContact : PFObject<PFSubclassing>

+ (NSString *)parseClassName;
- (void) setDetails:(id) detail;
+ (NSString *)personRef;

@property (retain) NSString *name;
@property (retain) NSString *address;
@property (retain) NSNumber *phoneNumber;
@property (retain) NSArray *children;

@end

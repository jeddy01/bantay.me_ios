//
//  EmergencyContact.m
//  BantayMe
//
//  Created by Oxygenventures iMac2 on 12/15/14.
//  Copyright (c) 2014 Oxygenventures iMac2. All rights reserved.
//

#import "EmergencyContact.h"
#import <Parse/PFObject+Subclass.h>

@implementation EmergencyContact

@dynamic name;
@dynamic address;
@dynamic phoneNumber;
@dynamic children;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Person";
}

- (void) setDetails:(id) detail{
    self.name = detail[@"name"];
    self.address = detail[@"address"];
    self.phoneNumber = detail[@"phoneNumber"];
    self.children = detail[@"children"];
}

+ (NSString *) personRef{
    return @"personRef";
}


@end
